import React, { useEffect, useState } from "react";
import { getClientIpAddress, getLocationByIpOrUrl } from "api/api";
import Geolocation from "api/models/Geolocation";
import LocationInfo from "components/LocationInfo";
import MapComponent from "components/MapComponent";
import { useLocation } from "react-router";
import { ClipLoader } from "react-spinners";
import history from "utils/history";
import SearchHistoryList from "components/SearchHistoryList";
import { toast } from "react-toastify";

const urlExpression = /^([a-zA-Z0-9]+(\.[a-zA-Z0-9]+)+.*)$/;
const ipAddressExpression = /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
const ipAddressRegex = new RegExp(ipAddressExpression);
const regex = /\\/g;
const urlRegex = new RegExp(urlExpression);

export const isResponseValid = (data: Geolocation): boolean => {
  return !!data.type;
};

const MainPage: React.FC = () => {
  const [isLoading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState<string>("");
  const [searchHistory, setSearchHistory] = useState<Geolocation[]>([]);
  const [userGeolocation, setUserGeolocation] = useState<Geolocation | null>(
    null
  );

  const [
    searchGeolocation,
    setSearchGeolocation,
  ] = useState<Geolocation | null>(null);

  const params = new URLSearchParams(useLocation().search);
  const value = params.get("value");

  useEffect(() => {
    (async () => {
      const ipResponse = await getClientIpAddress();
      if (ipResponse.status === "success") {
        const geolocationResponse = await getLocationByIpOrUrl(
          ipResponse.data.ip
        );
        if (geolocationResponse.status === "success") {
          setUserGeolocation(geolocationResponse.data);
        }
      }
      if (value) {
        await searchIpAddress(value);
      }
    })();
  }, [value]);

  const searchIpAddress = async (searchQuery: string) => {
    setLoading(true);
    if (searchQuery !== value) {
      setSearchValue(searchQuery);
      const response = await getLocationByIpOrUrl(searchQuery);

      if (response.status === "success" && isResponseValid(response.data)) {
        params.set("value", searchQuery);
        history.push({
          search: params.toString(),
        });
        setSearchGeolocation(response.data);
        setSearchHistory([response.data, ...searchHistory]);
      } else {
        toast.error("Nothing was found.");
      }
    }
    setLoading(false);
  };

  const handleItemFromHistoryClick = async (ipAddress: string) => {
    await searchIpAddress(ipAddress);
  };

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (searchValue.match(urlRegex) || searchValue.match(ipAddressRegex)) {
      await searchIpAddress(searchValue.replace(regex, "\\\\"));
    } else {
      toast.error("Wrong search format.");
    }
  };

  return (
    <div className="main-container">
      <div className="previous-search-container">
        <SearchHistoryList
          searchHistory={searchHistory}
          handleClick={handleItemFromHistoryClick}
        />
      </div>
      <div className="user-location__map">
        <MapComponent
          latitude={userGeolocation && userGeolocation.latitude}
          longitude={userGeolocation && userGeolocation.longitude}
          message="Map with user location"
        />
      </div>
      <div className="user-location__info">
        <LocationInfo geolocation={userGeolocation} title="User information" />
      </div>
      <form onSubmit={handleSubmit} className="search__form">
        <div className="search__field">
          <input
            placeholder="ip | url"
            type="search"
            onChange={handleSearch}
            value={searchValue}
            required
            className="custom-input"
          />
          <button type="submit" className="custom-button">
            {isLoading ? <ClipLoader size={10} color="#fff" /> : "Search"}
          </button>
        </div>
      </form>
      <div className="search__map">
        <MapComponent
          latitude={searchGeolocation && searchGeolocation.latitude}
          longitude={searchGeolocation && searchGeolocation.longitude}
        />
      </div>
      <div className="search__info">
        <LocationInfo
          geolocation={searchGeolocation}
          message="Information about last search"
          title="Search information"
        />
      </div>
    </div>
  );
};

export default MainPage;
