import axios, { AxiosResponse } from "axios";
import client from "api/axios";
import {
  parseToErrorResponse,
  parseToSuccessResponse,
  GetResponse,
} from "api/models/Envelope";
import Geolocation from "api/models/Geolocation";

const api = {
  searchByIP: (ipAddress: string): Promise<AxiosResponse<Geolocation>> => {
    return client.get(`/${ipAddress}`);
  },
  fetchUserIP: (): Promise<AxiosResponse<{ ip: string }>> => {
    return axios.get("https://api.ipify.org/?format=json");
  },
};

export const getClientIpAddress = async (): Promise<
  GetResponse<{ ip: string }>
> => {
  try {
    const response = await api.fetchUserIP();
    return parseToSuccessResponse(response.data);
  } catch (error) {
    return parseToErrorResponse(error.data);
  }
};

export const getLocationByIpOrUrl = async (
  ipAddress: string
): Promise<GetResponse<Geolocation>> => {
  try {
    const response = await api.searchByIP(ipAddress);
    return parseToSuccessResponse(response.data);
  } catch (error) {
    return parseToErrorResponse(error.data);
  }
};

export default api;
