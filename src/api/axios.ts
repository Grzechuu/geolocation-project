import axios from "axios";
import * as dotenv from "dotenv";

dotenv.config();

const client = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

client.interceptors.request.use((config) => {
  // config.headers.accept =
  //   "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
  // config.headers["Accept-Encoding"] = "gzip, deflate";
  // config.headers["Accept-Language"] = "pl,en-US;q=0.7,en;q=0.3";
  // config.headers["Host"] = "api.ipstack.com";
  // config.headers["Connection"] = "keep-alive";
  config.params = {
    ...config.params,
    access_key: process.env.REACT_APP_ACCESS_KEY,
  };
  return config;
});

export default client;
