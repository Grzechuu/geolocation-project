interface SuccessResponse<T> {
  status: "success";
  data: T;
}

interface ErrorResponse<T> {
  status: "failure";
  data: ErrorResponseData<T>;
  error_message: string;
}

type ErrorResponseData<T> = {
  [P in keyof Required<T>]: string | undefined;
} & {
  non_field_errors: string | undefined;
};

interface Error<T = { detail: string | undefined }> {
  status: "failure";
  data: T;
}

export type PostResponse<TRequest, TResponse = TRequest> =
  | SuccessResponse<TResponse>
  | ErrorResponse<TRequest>;

export type GetResponse<TResponse> = SuccessResponse<TResponse> | Error;

export const parseToErrorResponse = <T>(
  error: ErrorResponseData<T>,
  error_message = "Błąd API"
): ErrorResponse<T> => {
  return { status: "failure", data: error, error_message };
};

export const parseToSuccessResponse = <T>(data: T): SuccessResponse<T> => {
  return { status: "success", data };
};
