import React from "react";
import { Route, Switch } from "react-router-dom";
import { Router } from "react-router";
import "App.scss";
import MainPage from "MainPage";
import history from "utils/history";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const App: React.FC = () => {
  return (
    <Router history={history}>
      <Switch>
        <div className="app">
          <Route path={"/"} component={MainPage} />
        </div>
      </Switch>
      <ToastContainer />
    </Router>
  );
};

export default App;
