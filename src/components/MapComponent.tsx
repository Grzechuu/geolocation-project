import React, { useEffect } from "react";
import { MapContainer, TileLayer, Marker, Popup, useMap } from "react-leaflet";

interface Position {
  longitude: number | null;
  latitude: number | null;
}

interface Props extends Position {
  message?: string;
}

const MapComponent: React.FC<Props> = ({
  latitude,
  longitude,
  message = "Information about user location",
}) => {
  if (longitude && latitude) {
    return (
      <MapContainer
        center={[latitude, longitude]}
        zoom={8}
        scrollWheelZoom={false}
      >
        <RecenterComponent latitude={latitude} longitude={longitude} />
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={[latitude, longitude]}>
          <Popup>
            A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
        </Marker>
      </MapContainer>
    );
  }
  return (
    <div className="location-info__message">
      <span>{message}</span>
    </div>
  );
};

type NoUndefinedField<T> = {
  [P in keyof T]-?: Exclude<T[P], null | undefined>;
};

const RecenterComponent: React.FC<NoUndefinedField<Position>> = (props) => {
  const { longitude, latitude } = props;
  const map = useMap();
  useEffect(() => {
    map.setView([latitude, longitude], 8);
  }, [longitude, latitude]);
  return null;
};

export default MapComponent;
