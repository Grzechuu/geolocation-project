import React from "react";
import Geolocation from "api/models/Geolocation";

interface Props {
  searchHistory: Geolocation[];
  handleClick: (ipAddress: string) => void;
}

const SearchHistoryList: React.FC<Props> = ({ searchHistory, handleClick }) => {
  return (
    <div className="search-history">
      <h3 style={{ textAlign: "center" }}>Search history</h3>
      {searchHistory.map((item, idx) => {
        return (
          <div
            key={idx}
            className="search-history__item"
            onClick={() => handleClick(item.ip)}
          >
            <span>{item.ip}</span>
            <span>{item.country_name}</span>
          </div>
        );
      })}
    </div>
  );
};

export default SearchHistoryList;
