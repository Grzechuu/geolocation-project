import React from "react";
import Geolocation from "api/models/Geolocation";

interface Props {
  geolocation: Geolocation | null;
  message?: string;
  title: string;
}

const LocationInfo: React.FC<Props> = ({
  geolocation,
  message = "Information about user location",
  title,
}) => {
  if (geolocation) {
    const {
      ip,
      country_name,
      city,
      region_name,
      location,
      longitude,
      latitude,
      type,
      country_code,
      region_code,
      zip,
    } = geolocation;
    return (
      <div>
        {console.log(geolocation)}
        <div style={{ textAlign: "center" }}>
          <h3>{title}</h3>
        </div>
        <div className="location-info">
          <span>IP address: {ip}</span>
          <span>Type: {type}</span>
          <span>Country code: {country_code}</span>
          <span>
            Country name: {country_name}
            {location.country_flag_emoji}
          </span>
          <span>Region code: {region_code}</span>
          <span>Region name: {region_name}</span>
          <span>City: {city}</span>
          <span>zip: {zip}</span>
          <span>Longitude: {longitude}</span>
          <span>Latitude: {latitude}</span>
        </div>
      </div>
    );
  } else {
    return (
      <div className="location-info__message">
        <span>{message}</span>
      </div>
    );
  }
};

export default LocationInfo;
